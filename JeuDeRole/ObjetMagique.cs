﻿using JeuDeRole.Interface;
using JeuDeRole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole
{
    public class ObjetMagique : Personnage, IEquiper
    {

        List<ObjetMagiques> objetMagiques = new List<ObjetMagiques>()
        {
            new ObjetMagiques
            {
                Nom = "armatus",
                Type = "Arme",
                Effet= "FO = + 20",
                Prix = 100
            },
            new ObjetMagiques
            {
                Nom = "baguettus",
                Type = "baguette",
                Effet= "PM = + 20",
                Prix = 200
            },
            new ObjetMagiques
            {
                Nom = "armurus",
                Type = "armure",
                Effet = "FO = + 20",
                Prix = 150
            },
            new ObjetMagiques
            {
                Nom = "vetus",
                Type = "vêtement",
                Effet = "FO = + 20",
                Prix = 50
            },
            new ObjetMagiques
            {
                Nom = "annubis",
                Type = "anneau",
                Effet = "PM =+ 20",
                Prix = 100
            },
            new ObjetMagiques
            {
                Nom = "capis",
                Type = "cape",
                Effet = "AG = +20",
                Prix = 100
            },
        };

        public ObjetMagique(int nbJoueurTotal, int nbJoueurPhysique, int nbJoueurOrdi, List<Personne> joueurs) : base(nbJoueurTotal, nbJoueurPhysique, nbJoueurOrdi, joueurs)
        {

        }
        public string equiper(string nom)
        {
            Console.WriteLine("Indiquer l'objet magique que votre personnage" + nom + "souhaite avoir :");
            foreach (var objet in objetMagiques)
            {
                Console.WriteLine(objet.Nom + " : " + objet.Effet+ " "+ objet.Prix + "$");
            }
            Console.WriteLine("*************");

            var objetChoisi = Console.ReadLine();
            Console.WriteLine("*************");

            foreach (var item in _joueurs)
            {
                if (item.Nom == nom)
                {
                    item.Objet_Magique_Possedees.Add(objetChoisi);
                }
            }


            return objetChoisi;
        }

        public override void setCaracteristique(string nom)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using JeuDeRole.Model;
using System;
using System.Collections.Generic;

namespace JeuDeRole
{
    public class Programm 
    {
        public static int _nbJoueurTotal { get; }
        public static int _nbJoueurPhysique { get; }
        public static int _nbJoueurOrdi { get; }
        public static List<Personne> personnes { get; }
        static void Main(string[] args)
        {
            Jeu jeu = new Jeu(_nbJoueurTotal, _nbJoueurPhysique, _nbJoueurOrdi, personnes);
            jeu.startJeu();

        }



    }
}

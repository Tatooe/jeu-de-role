﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole
{
    public interface IProfession 
    {
        public string creerProfession(string nom);
        public string creerRandomProfession(string nom);
    }
}

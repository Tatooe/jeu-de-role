﻿using JeuDeRole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole
{
    public class FabriqueRace : Personnage
    {
        List<string> races = new List<string>()
        {
            "humain",
            "elfe",
            "hobbit",
            "orque",
            "gobelin",
            "dragon"
        };

        public FabriqueRace(int nbJoueurTotal, int nbJoueurPhysique, int nbJoueurOrdi, List<Personne> joueurs) : base(nbJoueurTotal, nbJoueurPhysique, nbJoueurOrdi, joueurs)
        {

        }

        //Ceation des races joueur physique
        public string creerRace(string nom)
        {

            Console.WriteLine("Indiquer la race de " + nom + " parmis la liste suivante: ");
            foreach (var race in races)
            {
                Console.WriteLine(race);
            }
            Console.WriteLine("*************");

            var raceChoisi = Console.ReadLine();
            Console.WriteLine("*************");

            foreach (var item in _joueurs)
            {
                if (item.Nom == nom)
                {
                    item.Race = raceChoisi;
                }
            }


            return raceChoisi;
        }


        //Ceation des races joueur ordi
        public string creerRandomRace()
        {
            var raceChoisi = "";

            foreach (var item in _joueurs)
            {
                if (item.estPhysique == false)
                {
                    var random = new Random();
                    int index = random.Next(races.Count);
                    raceChoisi = races[index];
                    item.Race = raceChoisi;
                }
            }

            return raceChoisi;

        }

        public override void setCaracteristique(string nom)
        {
            foreach (var item in _joueurs)
            {
                if (item.Nom == nom)
                {
                    switch (item.Race)
                    {
                        case "humain":
                            item.Caracteristique = new Caracteristique
                            {
                                PV = 100,
                                RA = 50,
                                AG = 50,
                                FO = 50,
                                DX = 40
                            };

                            break;
                        case "elfe":
                            item.Caracteristique = new Caracteristique
                            {
                                PV = 90,
                                RA = 80,
                                AG = 70,
                                FO = 40,
                                DX = 90
                            };
                            break;
                        case "hobbit":
                            item.Caracteristique = new Caracteristique
                            {
                                PV = 80,
                                RA = 100,
                                AG = 80,
                                FO = 40,
                                DX = 90
                            };
                            break;
                        case "orque":
                            item.Caracteristique = new Caracteristique
                            {
                                PV = 80,
                                RA = 70,
                                AG = 80,
                                FO = 90,
                                DX = 90
                            };
                            break;
                        case "gobelin":
                            item.Caracteristique = new Caracteristique
                            {
                                PV = 80,
                                RA = 70,
                                AG = 80,
                                FO = 90,
                                DX = 90
                            };                            
                            break;
                        case "dragon":
                            item.Caracteristique = new Caracteristique
                            {
                                PV = 100,
                                RA = 70,
                                AG = 80,
                                FO = 90,
                                DX = 60
                            };
                            break;
                    }
                }
            }
        }
    }
}

﻿using JeuDeRole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole
{
    public class ProfessionMagique : Personnage, IProfession
    {
        public ProfessionMagique(int nbJoueurTotal, int nbJoueurPhysique, int nbJoueurOrdi, List<Personne> joueurs) : base(nbJoueurTotal, nbJoueurPhysique, nbJoueurOrdi, joueurs)
        {

        }

        public List<string> professionM = new List<string>()
        {
            "magicien",
            "clerc",
            "monstre"
        };

        

        public string creerProfession(string nom)
        {
            

            Console.WriteLine("Quelle profession voulez-vous pour le personnage " + nom + "?");
            foreach(var item in professionM)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("***************");
            var professionChoisie = Console.ReadLine();
            Console.WriteLine("***************");
            foreach (var item in _joueurs)
            {
                if (item.Nom == nom)
                {
                    item.Profession = professionChoisie;
                }
            }

            return professionChoisie;
        }

        public string creerRandomProfession(string nom)
        {
            var professionChoisi = "";

            foreach (var item in _joueurs)
            {
                if (item.estPhysique == false && item.Nom == nom)
                {
                    var random = new Random();
                    int index = random.Next(professionM.Count);
                    professionChoisi = professionM[index];
                    item.Profession = professionChoisi;
                }
            }

            return professionChoisi;
        }


        public override void setCaracteristique(string nom)
        {
            foreach (var item in _joueurs)
            {
                if (item.Nom == nom)
                {
                    switch (item.Profession)
                    {
                        case "magicien":
                            item.Caracteristique.PM = 100;
                            break;
                        case "clerc":
                            item.Caracteristique.PM = 90;
                            break;
                        case "monstre":
                            item.Caracteristique.PM = 80;
                            break;
                    }
                }
            }
        }
    }
}

﻿using JeuDeRole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole
{
    public class Jeu
    {
        public static int _nbJoueurTotal { get; set; }
        public static int _nbJoueurPhysique { get; set; }
        public static int _nbJoueurOrdi { get; set; }

        public static List<Personne> _joueurs;

        public Jeu(int nbJoueurTotal, int nbJoueurPhysique, int nbJoueurOrdi, List<Personne> joueurs)
        {
            _nbJoueurTotal = nbJoueurTotal;
            _nbJoueurPhysique = nbJoueurPhysique;
            _nbJoueurOrdi = nbJoueurOrdi;
            _joueurs = joueurs;

        }

        public void startJeu()
        {
            Console.WriteLine("******Bienvenu dans le Jeu de rôle!******");

            getNbJoueur();

            getNomPerso();

            foreach (var personne in _joueurs)
            {
                choisirRace(personne.Nom);
                choisirProfession(personne.Nom);

            }

            resumChoixJoueur();

            return;
        }

        //Initialisation du nombre de joueurs
        public void getNbJoueur()
        {
            Console.WriteLine("Nombre de joueur: ");
            _nbJoueurTotal = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Nombre de joueur Physique: ");
            _nbJoueurPhysique = Convert.ToInt32(Console.ReadLine());

            _nbJoueurOrdi = _nbJoueurTotal - _nbJoueurPhysique;

            new Jeu(_nbJoueurTotal, _nbJoueurPhysique, _nbJoueurOrdi, _joueurs);


            Console.WriteLine("Le nombre de joueur simulé par l'ordinateur est de: " + _nbJoueurOrdi);
            Console.WriteLine("*************");
            Console.WriteLine("");
        }

        //Creation des noms de personnages
        public List<Personne> getNomPerso()
        {
            var personnages = Personnage.getNamePerso();

            return personnages;
        }

        

        //Ceation des races
        public string choisirRace(string nom)
        {
            FabriqueRace fabriqueRace = new FabriqueRace(_nbJoueurTotal, _nbJoueurPhysique, _nbJoueurOrdi, _joueurs);
            var raceChoisi = "";


            foreach (var personne in _joueurs)
            {

                if (personne.estPhysique == true && personne.Nom == nom && personne.Race == null)
                {
                    fabriqueRace.creerRace(personne.Nom);
                    fabriqueRace.setCaracteristique(personne.Nom);
                    raceChoisi = personne.Race;
                    Console.WriteLine();

                }
                else
                {
                    fabriqueRace.creerRandomRace();
                    fabriqueRace.setCaracteristique(personne.Nom);
                    raceChoisi = personne.Race;

                }

            }
            return raceChoisi;
        }

        public string choisirProfession(string nom)
        {
            ProfessionMagique professionMagique = new ProfessionMagique(_nbJoueurTotal, _nbJoueurPhysique, _nbJoueurOrdi, _joueurs);
            ProfessionOrdinaire professionOrdinaire = new ProfessionOrdinaire(_nbJoueurTotal, _nbJoueurPhysique, _nbJoueurOrdi, _joueurs);
            IProfession profM = new ProfessionMagique(_nbJoueurTotal, _nbJoueurPhysique, _nbJoueurOrdi, _joueurs);
            IProfession profO = new ProfessionOrdinaire(_nbJoueurTotal, _nbJoueurPhysique, _nbJoueurOrdi, _joueurs);

            var professionChoisi = "";

            foreach (var personne in _joueurs)
            {

                if (personne.estPhysique == true && personne.Nom == nom)
                {

                    choisirRace(personne.Nom);
                    Console.WriteLine();

                    Console.WriteLine("Souhaitez-vous utiliser de la magie avec votre personnage? O/N ");
                    var response = Console.ReadLine();
                    if (response == "O" || response == "o")
                    {
                        profM.creerProfession(personne.Nom);
                        professionMagique.setCaracteristique(personne.Nom);
                        professionChoisi = personne.Profession;
                    }
                    else
                    {
                        profO.creerProfession(personne.Nom);
                        professionOrdinaire.setCaracteristique(personne.Nom);
                        professionChoisi = personne.Profession;
                    }

                }
                else
                {

                    var random = new Random();
                    int index = random.Next(2);

                    if (index == 0)
                    {
                        profM.creerRandomProfession(personne.Nom);
                        professionMagique.setCaracteristique(personne.Nom);
                        professionChoisi = personne.Profession;
                    }
                    else
                    {
                        profO.creerRandomProfession(personne.Nom);
                        professionOrdinaire.setCaracteristique(personne.Nom);
                        professionChoisi = personne.Profession;
                    }

                }

            }
            return professionChoisi;
        }


        public void resumChoixJoueur()
        {
            Console.WriteLine("Ci-dessous le résumé de vos choix:");
            foreach (var nom in _joueurs)
            {
                Console.WriteLine("*************");
                Console.WriteLine("Nom Joueur:" + nom.Nom);

                Console.WriteLine("Race: " + nom.Race);
                Console.WriteLine("Profession: " + nom.Profession);
                Console.WriteLine("");
                Console.WriteLine("Caractéristiques :");
                Console.WriteLine("PV :" + nom.Caracteristique.PV);
                Console.WriteLine("RA :" + nom.Caracteristique.RA);
                Console.WriteLine("AG :" + nom.Caracteristique.AG);
                Console.WriteLine("FO :" + nom.Caracteristique.FO);
                Console.WriteLine("DX :" + nom.Caracteristique.DX);
                Console.WriteLine("PM :" + nom.Caracteristique.PM);
                Console.WriteLine("Argent :" + nom.Caracteristique.Argent);
                Console.WriteLine("*************");
            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole.Model
{
    public class Caracteristique
    {
        public int PV { get; set; }
        public int RA { get; set; }
        public int AG { get; set; }
        public int FO { get; set; }
        public int DX { get; set; }
        public int PM { get; set; }
        public int Argent { get; set; } = 1000;
    }

   
}

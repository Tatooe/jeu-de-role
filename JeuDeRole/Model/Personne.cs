﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole.Model
{
    public class Personne
    {
        public string Nom { get; set; }

        public bool estPhysique { get; set; }
        public string Race { get; set; }
        public string Profession { get; set; }
        public string Armure_possedee { get; set; }
        public List<string> Objet_Magique_Possedees  { get; set; }
        public List<string> Armes_Possedees { get; set; }
        public Caracteristique Caracteristique { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole.Model
{
    public class ObjetMagiques
    {
        public string Nom { get; set; }
        public string Type { get; set; }
        public string Effet { get; set; }
        public int NbCharge { get; set; }
        public int Prix { get; set; }
    }
}

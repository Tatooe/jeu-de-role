﻿using JeuDeRole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole
{
    public abstract class Personnage :Jeu
    {
        
        
        public Personnage(int nbJoueurTotal, int nbJoueurPhysique, int nbJoueurOrdi, List<Personne> joueurs) : base(nbJoueurTotal, nbJoueurPhysique, nbJoueurOrdi, joueurs)
        {
            
        }

        //Creation des noms de personnages
        public static List<Personne> getNamePerso()
        {
            _joueurs = new List<Personne>();
            for(int i = 0; i< _nbJoueurPhysique; i++)
            {
                Console.WriteLine("Indiquer le nom du joueur " + (i + 1));
                var nom = Console.ReadLine();

                _joueurs.Add(new Personne {
                    Nom = nom,
                    estPhysique = true
                }) ;
            }

            for(int i = 0; i < _nbJoueurOrdi; i++)
            {
                _joueurs.Add(new Personne { 
                    Nom = "Ordi" + (i + 1),
                    estPhysique = false
                });
            }

            return _joueurs;
        }

        public abstract void setCaracteristique(string nom);


    }
 }
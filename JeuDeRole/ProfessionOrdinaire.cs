﻿using JeuDeRole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeRole
{
    class ProfessionOrdinaire : Personnage, IProfession
    {
        public ProfessionOrdinaire(int nbJoueurTotal, int nbJoueurPhysique, int nbJoueurOrdi, List<Personne> joueurs) : base(nbJoueurTotal, nbJoueurPhysique, nbJoueurOrdi, joueurs)
        {

        }

        public List<string> professionO = new List<string>()
        {
            "guerrier",
            "voleur",
            "assassin"
        };



        public string creerProfession(string nom)
        {


            Console.WriteLine("Quelle profession voulez-vous pour le personnage " + nom + "?");
            foreach (var item in professionO)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("***************");
            var professionChoisie = Console.ReadLine();
            Console.WriteLine("***************");

            foreach (var item in _joueurs)
            {
                if (item.Nom == nom)
                {
                    item.Profession = professionChoisie;
                }
            }
            return professionChoisie;
        }

        public string creerRandomProfession(string nom)
        {
            var professionChoisi = "";

            foreach (var item in _joueurs)
            {
                if (item.estPhysique == false && item.Nom == nom)
                {
                    var random = new Random();
                    int index = random.Next(professionO.Count);
                    professionChoisi = professionO[index];
                    item.Profession = professionChoisi;
                }
            }

            return professionChoisi;
        }

        public override void setCaracteristique(string nom)
        {
            foreach (var item in _joueurs)
            {
                if (item.Nom == nom)
                {
                    switch (item.Profession)
                    {
                        case "guerrier":
                            item.Caracteristique.PV += 20;
                            break;
                        case "voleur":
                            item.Caracteristique.Argent += 100;
                            break;
                        case "assassin":
                            item.Caracteristique.DX += 20;
                            break;
                    }
                }
            }
        }

    }
}
